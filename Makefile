SOURCES  = $(shell ls *.c)
BIN = ${SOURCES:.c=}

all: $(BIN)

%: %.c
	mpicc -Wall -Wextra -o $@ $+

clean:
	rm -f $(BIN)
