/**************************************************************************
  Copyright (c) 2014 Intel Corporation

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 **************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <mpi.h>
#include <string.h>
#include <math.h>

#define MESSAGE(s) do { fprintf (stderr, "%d: %s line %d: %s\n", getpid(), __FILE__, __LINE__, s); \
                        fflush(stderr); } while(0)

char errbuf [2048];

#define TABLE_SIZE (256*64)
#define TOTAL_DATA_SIZE 256
#define NUM_CONST 10
#define NITER 1000
#define MIN(a,b) (double) (((a)<(b))?(a):(b))
#define FLAG_FEDERATION 0x0000000000000001LL
#define FLAG_KLINGON    0x0000000000000002LL

struct starship {
    long long flags;
    double x;  // position in x dimension
    double y;  // position in y dimension
    double z;  // position in z dimension
};

double interval_rand (double x0, double x1)
{
    return x0 + (x1 - x0) * rand() / ((double) RAND_MAX);
}

int main (int argc, char *argv[])
{
    int my_rank, size;
    int i, icnt, cidx;
    double delta, threshold, distance, dx, dy, dz;
    struct starship me;
    struct starship *other = NULL;
    int local_size, klingon_count;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    //
    // All processes allocate and initialize a table
    //
    MPI_Alloc_mem ((MPI_Aint) sizeof(struct starship) * TABLE_SIZE, MPI_INFO_NULL, &other);
    delta = 1./(double) TOTAL_DATA_SIZE;
    srand (1);
    for (i = 0; i < TABLE_SIZE; i++) {
        other [i].x = interval_rand (0.0, 1.0);
        other [i].y = interval_rand (0.0, 1.0);
        other [i].z = interval_rand (0.0, 1.0);
        other [i].flags = 0;
        if ((i % 2) == 0) {
            other [i].flags |= FLAG_KLINGON;
        } else {
            other [i].flags |= FLAG_FEDERATION;
        }
        //sprintf (errbuf, "%f %f %f", other [i].x, other [i].y, other [i].z);
        //MESSAGE (errbuf);
    }

    local_size = TOTAL_DATA_SIZE / size;
    delta = 1./TOTAL_DATA_SIZE;
    me.x = my_rank * local_size * delta;
    me.y = my_rank * local_size * delta;
    me.z = my_rank * local_size * delta;

    sprintf (errbuf, "Process %d starting position: %f %f %f, end is %f %f %f delta = %f",
             my_rank,
             me.x, me.y, me.z,
             (my_rank + 1) * local_size * delta,
             (my_rank + 1) * local_size * delta,
             (my_rank + 1) * local_size * delta,
             delta);
    MESSAGE (errbuf);

    //
    // iterate to update x position
    //

    klingon_count = 0;
    for (icnt = 0; icnt < local_size; icnt++) {
        threshold = 0.025;
        // lookup table and find any Klingons within the threshold
        cidx = -1;
        for (i = 0; i < TABLE_SIZE; i++) {
            dx = me.x - other[i].x;
            dy = me.y - other[i].y;
            dz = me.z - other[i].z;
            distance = sqrt ((dx * dx) + (dy * dy) + (dz * dz));
            // sprintf (errbuf, "%d %d %f %f %f %f %f %f %f %f", icnt, i,
            //          me.x, me.y, me.z, other[i].x, other[i].y, other[i].z, distance, threshold);
            // MESSAGE (errbuf);
            if (distance < threshold) {
                if ((other[i].flags & FLAG_KLINGON) != 0) {
                    klingon_count++;
                    //sprintf (errbuf, "Process %d Klingon at index %d loc = %f %f %f distance = %f thresh = %f",
                    //         my_rank, i, other[i].x, other[i].y, other[i].z, distance, threshold);
                    //MESSAGE (errbuf);
                }
            }
        }
        me.x = me.x + delta;
        me.y = me.y + delta;
        me.z = me.z + delta;
    }
    sprintf (errbuf, "Process %d starting position: %f, end is %f number of Klingons seen = %d",
             my_rank, my_rank * local_size * delta, (my_rank + 1) * local_size * delta, klingon_count);
    MESSAGE (errbuf);

    if (my_rank == 0) MPI_Free_mem(other);
    MPI_Finalize();
    return 0;
}
